--[[
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]]

-- general
lvim.log.level = "warn"
lvim.format_on_save = true
vim.opt.colorcolumn = "88"
vim.opt.list = true
vim.opt.listchars:append("eol:↴")
vim.opt.listchars:append("extends:❯")
vim.opt.listchars:append("nbsp:.")
vim.opt.listchars:append("tab:› ")
vim.opt.listchars:append("trail:•")

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
-- make splits the same size
lvim.builtin.which_key.mappings["="] = { "<C-w>=<cr>", "Even splits" }
-- tab navigation
lvim.keys.normal_mode["<M-h>"] = "gT"
lvim.keys.normal_mode["<M-j>"] = ":tabmove -1<cr>"
lvim.keys.normal_mode["<M-k>"] = ":tabmove +1<cr>"
lvim.keys.normal_mode["<M-l>"] = "gt"
-- telescope
lvim.keys.normal_mode["<C-p>"] = ":Telescope<cr>"
lvim.builtin.which_key.mappings["f"] = { ":Telescope find_files hidden=true<cr>", "My find file" }
-- undotree
lvim.builtin.which_key.mappings["u"] = { ":UndotreeToggle<cr>", "Undo tree" }
-- zen-mode
lvim.builtin.which_key.mappings["Z"] = { ":ZenMode<cr>", "Zen Mode" }

-- UI mappings
lvim.builtin.which_key.mappings["U"] = {
	name = "+UI",
	d = { "<cmd>set background=dark<cr>", "Background: dark" },
	l = { "<cmd>set background=light<cr>", "Background: light" },
	c = {
		name = "colorscheme",
		d = { "<cmd>colorscheme dayfox<cr>", "dayfox" },
		l = {
			"<cmd>let catppuccin_flavour='latte' | colorscheme catppuccin<cr>",
			"catppuccin: latte",
		},
		f = {
			"<cmd>let catppuccin_flavour='frappe' | colorscheme catppuccin<cr>",
			"catppuccin: frappe",
		},
		n = { "<cmd>colorscheme nordfox<cr>", "nordfox" },
	},
}

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.bufferline.options.mode = "tabs"
-- https://github.com/LunarVim/LunarVim/issues/3294
-- lvim.builtin.notify.active = false
lvim.builtin.terminal.active = false
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = true

local telescope = require("telescope")
telescope.setup({
	defaults = {
		layout_strategy = "horizontal",
	},
	pickers = {
		find_files = {
			preview = true,
		},
		git_files = {
			preview = true,
		},
		live_grep = {
			hidden = true,
		},
	},
})

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
	"bash",
	"c",
	"css",
	"dockerfile",
	"html",
	"java",
	"javascript",
	"json",
	"lua",
	"python",
	"rst",
	"rust",
	"scss",
	"tsx",
	"typescript",
	"vim",
	"vue",
	"yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true

-- generic LSP settings

-- ---@usage disable automatic installation of servers
-- lvim.lsp.automatic_servers_installation = false

-- ---configure a server manually. !!Requires `:LvimCacheReset` to take effect!!
-- ---see the full default list `:lua print(vim.inspect(lvim.lsp.automatic_configuration.skipped_servers))`
-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pyright", opts)
require("lvim.lsp.manager").setup("emmet_ls")
require("lvim.lsp.manager").setup("esbonio")

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skiipped for the current filetype
-- vim.tbl_map(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

lvim.lsp.null_ls.setup = {
	debug = true,
}

-- set a formatter, this will override the language server formatting capabilities (if it exists)
-- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
local formatters = require("lvim.lsp.null-ls.formatters")
formatters.setup({
	{ command = "black", filetypes = { "python" } },
	{ command = "isort", filetypes = { "python" } },
	{ command = "djlint", extra_args = { "--reformat", "-" }, filetypes = { "htmldjango" } },
	{
		command = "prettier",
		filetypes = { "javascript", "markdown", "typescript" },
	},
	{ command = "stylua", filetypes = { "lua" } },
})

-- set additional linters
-- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
local linters = require("lvim.lsp.null-ls.linters")
linters.setup({
	{ command = "djlint", filetypes = { "htmldjango" } },
})

-- Additional Plugins
lvim.plugins = {
	{
		"catppuccin/nvim",
		as = "catppuccin",
	},
	{
		"EdenEast/nightfox.nvim",
		config = function()
			require("nightfox").setup({
				options = {
					styles = {
						comments = "italic",
						keywords = "bold",
						functions = "italic,bold",
					},
					inverse = {
						match_paren = true,
					},
				},
			})
		end,
	},
	{
		"ethanholz/nvim-lastplace",
		event = "BufRead",
		config = function()
			require("nvim-lastplace").setup({
				lastplace_ignore_buftype = { "quickfix", "nofile", "help" },
				lastplace_ignore_filetype = {
					"gitcommit",
					"gitrebase",
					"svn",
					"hgcommit",
				},
				lastplace_open_folds = true,
			})
		end,
	},
	{
		"folke/zen-mode.nvim",
		cmd = "ZenMode",
		event = "BufRead",
		config = function()
			require("zen-mode").setup({
				window = {
					backdrop = 1,
					width = 100,
					height = 0.95,
					options = {
						signcolumn = "no",
						number = true,
						relativenumber = false,
					},
				},
				plugins = {
					gitsigns = { enabled = true },
				},
			})
		end,
	},
	{
		"iamcco/markdown-preview.nvim",
		run = "cd app && npm install",
		ft = "markdown",
		config = function()
			vim.g.mkdp_auto_start = 1
		end,
	},
	{ "mbbill/undotree" },
	{ "tpope/vim-repeat" },
	{ "tpope/vim-surround" },
	{
		"tzachar/cmp-tabnine",
		run = "./install.sh",
		requires = "hrsh7th/nvim-cmp",
		event = "InsertEnter",
		config = function()
			require("cmp_tabnine.config").setup({
				max_lines = 500,
				max_num_results = 5,
				sort = true,
				run_on_every_keystroke = true,
				snippet_placeholder = "…",
				ignored_file_types = {
					-- default is not to ignore
				},
				show_prediction_strength = true,
			})
		end,
	},
}

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
-- set file types
local au_filetypes = vim.api.nvim_create_augroup("filetypes", { clear = true })
vim.api.nvim_create_autocmd("BufEnter", {
	pattern = { "*.njk" },
	command = "setfiletype htmldjango",
	group = au_filetypes,
})

local au_load_save = vim.api.nvim_create_augroup("load_save", { clear = true })
-- load mofidied files on gaining focus
vim.api.nvim_create_autocmd("FocusGained,BufEnter", {
	pattern = "*",
	command = "!",
	group = au_load_save,
})
-- save all files on losing focus
vim.api.nvim_create_autocmd("FocusLost,WinLeave", {
	pattern = "*",
	command = "wa",
	group = au_load_save,
})

-- change theme according to day/night
local hr = tonumber(os.date("%H", os.time()))
if hr > 8 and hr < 18 then
	lvim.colorscheme = "catppuccin-latte"
else
	lvim.colorscheme = "catppuccin-frappe"
end
