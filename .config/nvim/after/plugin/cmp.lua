local cmp = require "cmp"

cmp.setup(
{
    snippet = {
        -- REQUIRED - you must specify a snippet engine
        expand = function(args)
            vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        end
    },
    mapping = {
        ['<Tab>'] = cmp.mapping(cmp.mapping.select_next_item(), { 'i', 's' }),
        ['<S-Tab>'] = cmp.mapping(cmp.mapping.select_prev_item(), { 'i', 's' })
    },
    sources = cmp.config.sources(
    {
        {name = "nvim_lsp"},
        {name = "vsnip"} -- For vsnip users.
    },
    {
        {name = "buffer"}
    }
    )
}
)

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(
"/",
{
    sources = {
        {name = "buffer"}
    }
}
)

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(
":",
{
    sources = cmp.config.sources(
    {
        {name = "path"}
    },
    {
        {name = "cmdline"}
    }
    )
}
)

-- Setup lspconfig.
-- Add additional capabilities supported by nvim-cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").update_capabilities(capabilities)

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local nvim_lsp = require "lspconfig"
local servers = {"vimls", "dockerls", "eslint", "pyright", "html"}
for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup {
        capabilities = capabilities
    }
end
