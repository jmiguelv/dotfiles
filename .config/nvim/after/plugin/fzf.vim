" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

if has('gui_running')
    let $FZF_DEFAULT_OPTS='--layout=reverse'
    let g:fzf_buffers_jump = 1
    noremap <C-p> :Files<cr>

    " include hidden files in Ag
    command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, '--nogroup --column --color --hidden', <bang>0)
endif
