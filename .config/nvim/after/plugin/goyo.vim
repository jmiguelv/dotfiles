" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

let g:goyo_width = 90
let g:goyo_height = 95

noremap <leader>c :Goyo<cr>
