" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

" enable basic formatting when a filetype is not found
"let g:neoformat_basic_format_align = 1
"let g:neoformat_basic_format_retab = 1
"let g:neoformat_basic_format_trim = 1

let g:neoformat_try_formatprg = 1

let g:neoformat_enabled_javascript = ['prettier']
let g:neoformat_enabled_python = ['isort', 'black']
let g:neoformat_enabled_vue = ['prettier']

let g:neoformat_run_all_formatters = 1
