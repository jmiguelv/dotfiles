" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

let g:netrw_altv = 1
let g:netrw_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_winsize = 50

let g:NetrwIsOpen=0

function! ToggleNetrw()
    if g:NetrwIsOpen
        let i = bufnr('$')
        while (i >= 1)
            if (getbufvar(i, '&filetype') == 'netrw')
                silent exe 'bwipeout ' . i 
            endif
            let i-=1
        endwhile
        let g:NetrwIsOpen=0
    else
        let g:NetrwIsOpen=1
        silent Vexplore
    endif
endfunction

noremap <leader>n :call ToggleNetrw()<cr>
