" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_python_checkers = ['flake8']
