require'nvim-treesitter.configs'.setup {
    highlight = {
        enable = true,
        disable = {},
    },
    indent = {
        enable = true,
        disable = {},
    },
    ensure_installed = {
        "css",
        "dockerfile",
        "html",
        "javascript",
        "json",
        "lua",
        "python",
        "rst",
        "scss",
        "typescript",
        "vim",
        "vue",
        "yaml"
    },
}
