" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

" auto set file types
augroup filetypes
    autocmd!
    " set htmldjango filetype
    autocmd BufNewFile,BufRead *.html set filetype=htmldjango
augroup END

" format on save
augroup fmt
    autocmd!
    "https://github.com/sbdchd/neoformat/issues/134
    "autocmd BufWritePre * undojoin | Neoformat
    autocmd BufWritePre * try | undojoin | Neoformat | catch /^Vim\%((\a\+)\)\=:E790/ | finally | silent Neoformat | endtry
augroup END

" indentation
augroup indentation
    autocmd!
    autocmd BufNewFile,BufRead *.js,*.html,*.css,*.sass,*.scss setlocal tabstop=2 softtabstop=2 shiftwidth=2
augroup END

" auto load/save files
augroup load_save
    autocmd!
    " load modified files on focus
    autocmd FocusGained,BufEnter * :silent! !
    " save all files on losing focus
    autocmd FocusLost,WinLeave * :silent! wa
augroup END

" auto toggle between line number modes
augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
