" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

let s:background = 0

function! ToggleBackground()
    if s:background
        set background=dark
        let s:background = 0
    else
        set background=light
        let s:background = 1
    endif
endfunction

let s:themes = ['nordfox', 'dawnfox', 'nord', 'gruvbox-material']
let s:selected_theme = 0

function! ToggleTheme()
    let s:selected_theme += 1
    if s:selected_theme >= len(s:themes)
        let s:selected_theme = 0
    endif

    execute 'colorscheme ' . s:themes[s:selected_theme]
endfunction
