" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

let g:python3_host_prog = '/usr/local/bin/python3'

" plugin manager {
runtime ./plug.vim
" }

" functions {
runtime ./functions.vim 
" }

let mapleader=','

" use clipboard as default register
if system('uname -s') == 'Darwin\n'
    set clipboard=unnamed 
else
    set clipboard=unnamedplus
endif

" ui
syntax on
set completeopt=menu,menuone,noselect
set t_Co=256
set termguicolors

let g:gruvbox_material_background = 'medium'

let g:nord_cursor_line_number_background = 1
let g:nord_italic = 1
let g:nord_italic_comments = 1

colorscheme nordfox

" file type recognition
filetype plugin indent on

set autoindent
set backspace=eol,indent,start
set colorcolumn=88
set cursorline
set expandtab
set foldmethod=marker
set foldnestmax=5
set nofoldenable
set history=10000
set hlsearch
set ignorecase
set incsearch
set list
set listchars=eol:¬,extends:❯,nbsp:.,tab:›\ ,trail:•
set mouse=a
set mousehide
set noshowmode
set notimeout
set nowrap
set number relativenumber
set ruler
set shiftwidth=4
set showcmd
set showmatch
set smartcase
set splitbelow
set splitright
set softtabstop=4
set spell
set tabstop=4
set undofile

" autocommands {
runtime ./autocmd.vim
" }

" mappings {
runtime ./mappings.vim
" }

" copilot {
runtime ./copilot.vim
" }

" plugins configuration sourced from ./after/plugin
