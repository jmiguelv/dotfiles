" vim set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

noremap <leader>ev :split $MYVIMRC<cr>
noremap <leader>sv :source $MYVIMRC<cr>
noremap <leader>pi :PlugClean<cr>:PlugInstall<cr>
noremap <leader>pu :PlugUpgrade<cr>:PlugUpdate<cr>:PlugClean<cr>

noremap <leader>[ :call ToggleBackground()<cr>
noremap <leader>] :call ToggleTheme()<cr>

noremap <leader>/ :Ag<cr>
" global search
noremap <leader>g :g//#<Left><Left>
noremap <leader>gb :BlamerToggle<cr>

" manual formatting
noremap <leader>f :Neoformat<cr>
noremap <leader>mf ggVG='.
noremap <leader>p} }kp<cr>
noremap <leader>u :UndotreeToggle<cr>

" copy current file path
noremap <leader>fp :let @+=expand("%")<cr>

" turn off search highlighting
noremap <silent> <leader><space> :nohlsearch<cr>

" make splits the same size
noremap <leader>= <c-w>=<cr>

" navigation {
" split windows
noremap <leader>- <C-w>s<C-w>j
noremap <leader>\| <C-w>v<C-w>l

" window navigation
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
nnoremap <Tab><Tab> <C-W>w

" tabs
noremap <M-h> gT
noremap <M-Left> gT 
noremap <M-l> gt
noremap <M-Right> gt
noremap <tab>h :tabmove -1<cr>
noremap <tab>l :tabmove +1<cr>
noremap <leader>< gT
noremap <leader>> gt

" visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv
