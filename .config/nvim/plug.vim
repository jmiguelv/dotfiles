" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker spell:

if !exists('g:vscode')
    call plug#begin('~/.vim/plugged')

    Plug 'APZelos/blamer.nvim'
    Plug 'chrisbra/csv.vim'
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
    Plug 'jiangmiao/auto-pairs'
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf.vim'
    Plug 'junegunn/goyo.vim'
    Plug 'mattn/emmet-vim'
    Plug 'mbbill/undotree'
    Plug 'mhinz/vim-signify'
    Plug 'mhinz/vim-startify'
    Plug 'nathanaelkane/vim-indent-guides'
    Plug 'neovim/nvim-lspconfig'
    Plug 'nvim-lualine/lualine.nvim'
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
    Plug 'psf/black', { 'branch': 'stable' }
    Plug 'ryanoasis/vim-devicons'
    Plug 'sbdchd/neoformat'
    Plug 'scrooloose/nerdcommenter'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-surround'
    "Plug 'vim-airline/vim-airline'
    "Plug 'vim-airline/vim-airline-themes'
    Plug 'vim-syntastic/syntastic'

    " colorschemes {
    Plug 'arcticicestudio/nord-vim'
    Plug 'EdenEast/nightfox.nvim'
    Plug 'sainnhe/gruvbox-material'
    " }

    " nvim-cmp {
    Plug 'neovim/nvim-lspconfig'
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'hrsh7th/cmp-buffer'
    Plug 'hrsh7th/cmp-path'
    Plug 'hrsh7th/cmp-cmdline'
    Plug 'hrsh7th/nvim-cmp'

    " vsnip
    Plug 'hrsh7th/cmp-vsnip'
    Plug 'hrsh7th/vim-vsnip'
    " }

    " telescope {
    Plug 'nvim-lua/plenary.nvim'
    "Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    " }

    " copilot {
    Plug 'github/copilot.vim'
    " }

    call plug#end()
endif
