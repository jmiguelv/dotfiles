" vim: set sw=4 ts=4 sts=4 et tw=88 foldmarker={,} foldlevel=0 foldmethod=marker

" plugin manager {
if !exists('g:vscode')
    call plug#begin('~/.vim/plugged')

    if has('nvim')
        Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins'  }
        Plug 'nvim-lua/plenary.nvim'
        Plug 'nvim-lua/popup.nvim'
        Plug 'nvim-telescope/telescope.nvim'
        Plug 'psf/black', { 'branch': 'stable' }
    else
        Plug 'Shougo/deoplete.nvim'
        Plug 'psf/black', { 'tag': '19.10b0' }
        Plug 'roxma/nvim-yarp'
        Plug 'roxma/vim-hug-neovim-rpc'
    endif

    Plug 'APZelos/blamer.nvim'
    Plug 'carlitux/deoplete-ternjs', { 'do': 'npm install -g tern' }
    Plug 'chrisbra/csv.vim'
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
    Plug 'jiangmiao/auto-pairs'
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf.vim'
    Plug 'junegunn/goyo.vim'
    Plug 'majutsushi/tagbar'
    Plug 'mattn/emmet-vim'
    Plug 'mbbill/undotree'
    Plug 'mhinz/vim-signify'
    Plug 'mhinz/vim-startify'
    Plug 'nathanaelkane/vim-indent-guides'
    Plug 'raimon49/requirements.txt.vim'
    Plug 'ryanoasis/vim-devicons'
    Plug 'sbdchd/neoformat'
    Plug 'scrooloose/nerdcommenter'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-surround'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'vim-syntastic/syntastic'
    Plug 'zchee/deoplete-jedi'

    " colorschemes
    Plug 'arcticicestudio/nord-vim'
    Plug 'sainnhe/gruvbox-material'
    call plug#end()
endif

let s:background = 0

function! ToggleBackground()
    if s:background
        set background=dark
        let s:background = 0
    else
        set background=light
        let s:background = 1
    endif
endfunction

let s:themes = ['nord', 'gruvbox-material']
let s:selected_theme = 0

function! ToggleTheme()
    let s:selected_theme += 1
    if s:selected_theme >= len(s:themes)
        let s:selected_theme = 0
    endif

    execute 'colorscheme ' . s:themes[s:selected_theme]
endfunction
" }

let g:python3_host_prog = '/usr/local/bin/python3'

let mapleader=','

" use clipboard as default register
if system('uname -s') == 'Darwin\n'
    set clipboard=unnamed 
else
    set clipboard=unnamedplus
endif

" ui
syntax on
set t_Co=256
set termguicolors

let g:gruvbox_material_background = 'medium'
colorscheme nord

" file type recognition
filetype plugin indent on

set autoindent
set backspace=eol,indent,start
set colorcolumn=88
set cursorline
set expandtab
set foldmethod=marker
set foldnestmax=5
set nofoldenable
set history=10000
set hlsearch
set ignorecase
set incsearch
set list
set listchars=eol:¬,extends:❯,nbsp:.,tab:›\ ,trail:•
set mouse=a
set mousehide
set noshowmode
set notimeout
set nowrap
set number relativenumber
set ruler
set shiftwidth=4
set showcmd
set showmatch
set smartcase
set splitbelow
set splitright
set softtabstop=4
set spell
set tabstop=4
set undofile

" autocommands {
" auto set file types
augroup filetypes
    autocmd!
    " set htmldjango filetype
    autocmd BufNewFile,BufRead *.html set filetype=htmldjango
augroup END

" format on save
augroup fmt
    autocmd!
    "https://github.com/sbdchd/neoformat/issues/134
    "autocmd BufWritePre * undojoin | Neoformat
    autocmd BufWritePre * try | undojoin | Neoformat | catch /^Vim\%((\a\+)\)\=:E790/ | finally | silent Neoformat | endtry
augroup END

" indentation
augroup indentation
    autocmd!
    autocmd BufNewFile,BufRead *.js,*.html,*.css,*.sass,*.scss setlocal tabstop=2 softtabstop=2 shiftwidth=2
augroup END

" auto load/save files
augroup load_save
    autocmd!
    " load modified files on focus
    autocmd FocusGained,BufEnter * :silent! !
    " save all files on losing focus
    autocmd FocusLost,WinLeave * :silent! wa
augroup END

" auto toggle between line number modes
augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
" }

" mappings {
noremap <leader>ev :split $MYVIMRC<cr>
noremap <leader>sv :source $MYVIMRC<cr>
noremap <leader>pi :PlugClean<cr>:PlugInstall<cr>
noremap <leader>pu :PlugUpgrade<cr>:PlugUpdate<cr>:PlugClean<cr>

noremap <leader>[ :call ToggleBackground()<cr>
noremap <leader>] :call ToggleTheme()<cr>

noremap <leader>/ :Ag<cr>
noremap <leader>f :Neoformat<cr>
" global search
noremap <leader>g :g//#<Left><Left>
noremap <leader>gb :BlamerToggle<cr>

" manual formatting
noremap <leader>mf ggVG='.
noremap <leader>p} }kp<cr>
noremap <leader>t :TagbarToggle<cr>
noremap <leader>u :UndotreeToggle<cr>

" copy current file path
noremap <leader>fp :let @+=expand("%")<cr>

" turn off search highlighting
noremap <silent> <leader><space> :nohlsearch<cr>

" make splits the same size
noremap <leader>= <c-w>=<cr>

" navigation {
" split windows
noremap <leader>- <C-w>s<C-w>j
noremap <leader>\| <C-w>v<C-w>l

" window navigation
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
nnoremap <Tab><Tab> <C-W>w

" tabs
noremap <M-h> gT
noremap <M-Left> gT 
noremap <M-l> gt
noremap <M-Right> gt
noremap <tab>h :tabmove -1<cr>
noremap <tab>l :tabmove +1<cr>
noremap <leader>< gT
noremap <leader>> gt
" }

" visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv
" }

" plugins {
" airline {
"let g:airline_theme='gruvbox'
" }

" deoplete {
let g:deoplete#enable_at_startup = 1
call deoplete#custom#option('num_processes', 2)


" enable tab
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
inoremap <silent><expr><tab> pumvisible() ? '\<c-n>' : '\<tab>'
" }

" fzf/telescope {
if has('gui_running')
    let $FZF_DEFAULT_OPTS='--layout=reverse'
    let g:fzf_buffers_jump = 1
    noremap <C-p> :Files<cr>

    " include hidden files in Ag
    command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, '--nogroup --column --color --hidden', <bang>0)
else
    noremap <C-p> <cmd>Telescope find_files<cr>
    nnoremap <leader>ff <cmd>Telescope find_files<cr>
    nnoremap <leader>fg <cmd>Telescope live_grep<cr>
    nnoremap <leader>fb <cmd>Telescope buffers<cr>
endif
" }

" goyo {
let g:goyo_width = 90
let g:goyo_height = 95

noremap <leader>c :Goyo<cr>
" }

" indent guides {
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
" }

" nerdtree {
let g:NERDTreeQuitOnOpen = 1
let g:NERDTreeShowHidden = 1
let g:NERDTreeMinimalUI = 1

let g:netrw_altv = 1
let g:netrw_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_winsize = 50

let g:NetrwIsOpen=0

function! ToggleNetrw()
    if g:NetrwIsOpen
        let i = bufnr('$')
        while (i >= 1)
            if (getbufvar(i, '&filetype') == 'netrw')
                silent exe 'bwipeout ' . i 
            endif
            let i-=1
        endwhile
        let g:NetrwIsOpen=0
    else
        let g:NetrwIsOpen=1
        silent Vexplore
    endif
endfunction

noremap <leader>n :call ToggleNetrw()<cr>
" }

" neoformat {
" enable basic formatting when a filetype is not found
"let g:neoformat_basic_format_align = 1
"let g:neoformat_basic_format_retab = 1
"let g:neoformat_basic_format_trim = 1

let g:neoformat_try_formatprg = 1

let g:neoformat_htmldjango_djlint = {
            \ 'exe': 'djlint',
            \ 'args': ['--quiet', '--reformat'],
            \ 'replace': 1
            \ }
let g:neoformat_enabled_htmldjango = ['djlint']

let g:neoformat_enabled_javascript = ['prettier']
let g:neoformat_enabled_python = ['isort', 'black']
let g:neoformat_enabled_vue = ['prettier']

let g:neoformat_run_all_formatters = 1
" }

" syntastic {
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_python_checkers = ['flake8']
" }
" }
" }
