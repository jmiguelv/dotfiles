# Zim configuration
ZIM_HOME=~/.zim

# Download zimfw plugin manager if missing.
if [[ ! -e ${ZIM_HOME}/zimfw.zsh ]]; then
  curl -fsSL --create-dirs -o ${ZIM_HOME}/zimfw.zsh \
      https://github.com/zimfw/zimfw/releases/latest/download/zimfw.zsh
fi

# Install missing modules, and update ${ZIM_HOME}/init.zsh if missing or outdated.
if [[ ! ${ZIM_HOME}/init.zsh -nt ${ZDOTDIR:-${HOME}}/.zimrc ]]; then
  source ${ZIM_HOME}/zimfw.zsh init -q
fi

# Vim mode
bindkey -v

# Initialize modules.
source ${ZIM_HOME}/init.zsh

# zsh-history-substring-search
zmodload -F zsh/terminfo +p:terminfo
# Bind ^[[A/^[[B manually so up/down works both before and after zle-line-init
for key ('^[[A' '^P' ${terminfo[kcuu1]}) bindkey ${key} history-substring-search-up
for key ('^[[B' '^N' ${terminfo[kcud1]}) bindkey ${key} history-substring-search-down
for key ('k') bindkey -M vicmd ${key} history-substring-search-up
for key ('j') bindkey -M vicmd ${key} history-substring-search-down
unset key

# User configuration
export HB=`/usr/local/bin/brew --prefix`
PYTHON_VERSION="3.9"
export PATH=$HOME/bin:$HOME/.local/bin:/usr/local/bin:/usr/local/sbin:$HB/opt/python@$PYTHON_VERSION/libexec/bin:/usr/local/opt/ruby/bin:$(gem environment gemdir)/bin:/usr/local/opt/openjdk@11/bin:$PATH

ssh-add --apple-use-keychain -q $HOME/.ssh/id_kdl
ssh-add --apple-use-keychain -q $HOME/.ssh/id_pmme

ulimit -n unlimited

export EDITOR=`which nvim`
export NVM_DIR="$HOME/.nvm"
export PIPENV_VENV_IN_PROJECT=1
#export JAVA_HOME=`/usr/libexec/java_home`

source $HOME/.aliases

test -f $HOME/.fzf.zsh && source $HOME/.fzf.zsh
test -f $HOME/.iterm2_shell_integration.zsh && source $HOME/.iterm2_shell_integration.zsh
#test -f $HOME/.travis/travis.sh && source $HOME/.travis/travis.sh
test -f $HB/bin/virtualenvwrapper.sh && source $HB/bin/virtualenvwrapper.sh

eval "$(zoxide init zsh)"

eval "$(starship init zsh)"
